library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_hdl is
    port (
        DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
        DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
        DDR_cas_n : inout STD_LOGIC;
        DDR_ck_n : inout STD_LOGIC;
        DDR_ck_p : inout STD_LOGIC;
        DDR_cke : inout STD_LOGIC;
        DDR_cs_n : inout STD_LOGIC;
        DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
        DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
        DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
        DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
        DDR_odt : inout STD_LOGIC;
        DDR_ras_n : inout STD_LOGIC;
        DDR_reset_n : inout STD_LOGIC;
        DDR_we_n : inout STD_LOGIC;
        FIXED_IO_ddr_vrn : inout STD_LOGIC;
        FIXED_IO_ddr_vrp : inout STD_LOGIC;
        FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
        FIXED_IO_ps_clk : inout STD_LOGIC;
        FIXED_IO_ps_porb : inout STD_LOGIC;
        FIXED_IO_ps_srstb : inout STD_LOGIC;
        
        CAM1_D : in STD_LOGIC_VECTOR ( 9 downto 0 );
        CAM1_HSYNC : in STD_LOGIC;
        CAM1_PCLK : in STD_LOGIC;
        CAM1_SCL : inout STD_LOGIC;
        CAM1_SDA : inout STD_LOGIC;
        CAM1_VSYNC : in STD_LOGIC;
        CAM1_XCLK : out STD_LOGIC;
        
        RESET : out STD_LOGIC
    );
end entity;

architecture structure of top_hdl is
begin

    i_top: entity work.top_wrapper
    port map (
        DDR_addr(14 downto 0)     => DDR_addr(14 downto 0),
        DDR_ba(2 downto 0)        => DDR_ba(2 downto 0),
        DDR_cas_n                 => DDR_cas_n,
        DDR_ck_n                  => DDR_ck_n,
        DDR_ck_p                  => DDR_ck_p,
        DDR_cke                   => DDR_cke,
        DDR_cs_n                  => DDR_cs_n,
        DDR_dm(3 downto 0)        => DDR_dm(3 downto 0),
        DDR_dq(31 downto 0)       => DDR_dq(31 downto 0),
        DDR_dqs_n(3 downto 0)     => DDR_dqs_n(3 downto 0),
        DDR_dqs_p(3 downto 0)     => DDR_dqs_p(3 downto 0),
        DDR_odt                   => DDR_odt,
        DDR_ras_n                 => DDR_ras_n,
        DDR_reset_n               => DDR_reset_n,
        DDR_we_n                  => DDR_we_n,
        FIXED_IO_ddr_vrn          => FIXED_IO_ddr_vrn,
        FIXED_IO_ddr_vrp          => FIXED_IO_ddr_vrp,
        FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
        FIXED_IO_ps_clk           => FIXED_IO_ps_clk,
        FIXED_IO_ps_porb          => FIXED_IO_ps_porb,
        FIXED_IO_ps_srstb         => FIXED_IO_ps_srstb,
        CAM1_D(9 downto 0)        => CAM1_D(9 downto 0),
        CAM1_HSYNC                => CAM1_HSYNC,
        CAM1_PCLK                 => CAM1_PCLK,
        CAM1_SCL                  => CAM1_SCL,
        CAM1_SDA                  => CAM1_SDA,
        CAM1_VSYNC                => CAM1_VSYNC,
        CAM1_XCLK                 => CAM1_XCLK,
        RESET                     => RESET
    );
end architecture;
