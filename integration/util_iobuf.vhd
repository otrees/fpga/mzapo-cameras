library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library unisim;
use unisim.vcomponents.all;

entity util_iobuf is
  port (
  I, T : in std_logic;
  IO : inout std_logic;
  O : out std_logic
        );
end util_iobuf;

architecture Behavioral of util_iobuf is

begin
    IOBUF_inst : IOBUF
generic map (
   DRIVE => 12,
   IOSTANDARD => "DEFAULT",
   SLEW => "SLOW")
port map (
   O => O,     -- Buffer output
   IO => IO,   -- Buffer inout port (connect directly to top-level port)
   I => I,     -- Buffer input
   T => T      -- 3-state enable input, high=input, low=output
);
end Behavioral;
