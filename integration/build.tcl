if {$tcl_platform(platform) eq "windows"} {
    set jobs [expr {$env(NUMBER_OF_PROCESSORS)/2}]
} else {
    set jobs [expr {[exec "nproc"]/2}]
}

puts "Using $jobs jobs."

open_project ./vtp/vtp.xpr
reset_run synth_1
reset_run impl_1

set design_file ./vtp/vtp.srcs/bd/top/top.bd
set obj [get_files $design_file]
generate_target all $obj
export_ip_user_files -of_objects $obj -no_script -force -quiet

update_compile_order -fileset sources_1
update_ip_catalog -rebuild -update_module_ref -scan_changes
upgrade_ip [get_ips]

launch_runs synth_1 -jobs $jobs
wait_on_run synth_1
launch_runs impl_1 -jobs $jobs
wait_on_run impl_1
launch_runs impl_1 -jobs $jobs -to_step write_bitstream
wait_on_run impl_1
file copy -force ./vtp/vtp.runs/impl_1/top_hdl.bit ./mzapo-cam.bit
