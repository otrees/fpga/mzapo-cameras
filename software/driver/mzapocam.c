#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>

#include <media/v4l2-async.h>
#include <media/v4l2-dev.h>
#include <media/v4l2-device.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-subdev.h>

#include <media/videobuf2-core.h>
#include <media/videobuf2-dma-contig.h>

struct mzapo_cam_device
{
    struct device* dev;
    struct video_device video_dev;
    struct v4l2_device v4l2_dev;
    struct v4l2_subdev* sensor_subdev;
    struct device_node* sensor_node;
    struct v4l2_async_notifier notifier;
    struct vb2_queue queue;
    struct mutex lock;
    void __iomem* dma_reg;
    void __iomem* cam_reg;
    bool is_open;
    // struct vb2_v4l2_buffer* curr_buf;
    // struct media_pad pad;
};

static int mzapo_cam_queue_setup(
  struct vb2_queue* vq, unsigned int* nbuffers, unsigned int* nplanes,
  unsigned int sizes[], struct device* alloc_devs[])
{
    struct mzapo_cam_device* camdev = vb2_get_drv_priv(vq);
    int ret;
    unsigned int img_size;

    struct v4l2_subdev_format subdev_fmt = {
      .which = 0,
    };

    ret =
      v4l2_subdev_call(camdev->sensor_subdev, pad, get_fmt, NULL, &subdev_fmt);
    if (ret < 0)
    {
        dev_err(camdev->dev, "failed to get format from subdevice\n");
        return ret;
    }

    img_size = subdev_fmt.format.width * subdev_fmt.format.height *
      2; // all formats are 16-bit

    if (*nplanes)
        return sizes[0] < img_size ? -EINVAL : 0;

    *nplanes = 1;
    sizes[0] = img_size;

    return 0;
}

static void mzapo_cam_buffer_queue(struct vb2_buffer* vb)
{
    // TODO
}

static irqreturn_t mzapo_cam_irq_handler(int irq, void* dev_id)
{
    struct mzapo_cam_device* cam_dev = dev_id;

    dev_info(cam_dev->dev, "Interrupt received\n");

    // TODO

    return IRQ_HANDLED;
}

static int mzapo_cam_start_streaming(struct vb2_queue* vq, unsigned int count)
{
    // TODO
}

static void mzapo_cam_stop_streaming(struct vb2_queue* vq)
{
    // TODO
}

static const struct vb2_ops mzapo_cam_queue_ops = {
  .queue_setup = mzapo_cam_queue_setup,
  .buf_queue = mzapo_cam_buffer_queue,
  .wait_prepare = vb2_ops_wait_prepare,
  .wait_finish = vb2_ops_wait_finish,
  .start_streaming = mzapo_cam_start_streaming,
  .stop_streaming = mzapo_cam_stop_streaming,
};

static int
mzapo_cam_querycap(struct file* file, void* fh, struct v4l2_capability* cap)
{
    struct mzapo_cam_device* camdev = video_drvdata(file);

    // TODO zkontrolovat flagy (nemohlo by tam treba byt i V4L2_CAP_ASYNCIO? - nezvladne to videobuf2 samo?)
    cap->device_caps =
      V4L2_CAP_STREAMING | V4L2_CAP_READWRITE | V4L2_CAP_VIDEO_CAPTURE;
    cap->capabilities = cap->device_caps | V4L2_CAP_DEVICE_CAPS;
    strlcpy(cap->driver, "mzapocam", sizeof(cap->driver));
    strlcpy(cap->card, camdev->video_dev.name, sizeof(cap->card));
    snprintf(
      cap->bus_info, sizeof(cap->bus_info), "platform:%s",
      camdev->sensor_node->full_name);
    return 0;
}

static int __mzapo_cam_get_format(
  struct mzapo_cam_device* camdev, struct v4l2_format* format)
{
    struct v4l2_subdev_format subdev_fmt = {
      .pad = 0,
    };
    int ret;
    struct v4l2_pix_format* pix = &format->fmt.pix;

    if (format->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
        return -EINVAL;

    ret =
      v4l2_subdev_call(camdev->sensor_subdev, pad, get_fmt, NULL, &subdev_fmt);
    if (ret < 0)
    {
        dev_err(camdev->dev, "failed to get format from subdevice\n");
        return ret;
    }

    pix->width = subdev_fmt.format.width;
    pix->height = subdev_fmt.format.height;
    pix->colorspace = subdev_fmt.format.colorspace;
    pix->field = subdev_fmt.format.field;
    pix->bytesperline = subdev_fmt.format.width * 2; // all formats are 16-bit
    pix->sizeimage = pix->bytesperline * pix->height;

    switch (subdev_fmt.format.code)
    {
        case MEDIA_BUS_FMT_RGB565_2X8_BE:
        case MEDIA_BUS_FMT_RGB565_2X8_LE:
            pix->pixelformat = V4L2_PIX_FMT_RGB565;
            break;
        case MEDIA_BUS_FMT_YUYV8_2X8:
            pix->pixelformat = V4L2_PIX_FMT_YUYV;
            break;
        case MEDIA_BUS_FMT_UYVY8_2X8:
            pix->pixelformat = V4L2_PIX_FMT_UYVY;
            break;
        case MEDIA_BUS_FMT_YVYU8_2X8:
            pix->pixelformat = V4L2_PIX_FMT_YVYU;
            break;
        case MEDIA_BUS_FMT_VYUY8_2X8:
            pix->pixelformat = V4L2_PIX_FMT_VYUY;
            break;
        default:
            dev_err(
              camdev->dev, "unknown media bus format: %x\n",
              subdev_fmt.format.code);
            return -EINVAL;
    }

    return 0;
}

static int
mzapo_cam_get_format(struct file* file, void* fh, struct v4l2_format* format)
{
    struct mzapo_cam_device* camdev = video_drvdata(file);
    int ret;

    ret = __mzapo_cam_get_format(camdev, format);

    return ret;
}

static int __mzapo_cam_set_format(
  struct mzapo_cam_device* camdev, struct v4l2_format* format, u32 which)
{
    struct v4l2_subdev_format subdev_fmt = {
      .which = which,
      .pad = 0,
    };
    struct v4l2_subdev_pad_config subdev_pad_config;
    struct v4l2_pix_format* pix = &format->fmt.pix;
    int ret = 0;

    subdev_fmt.format.width = pix->width;
    subdev_fmt.format.height = pix->height;
    subdev_fmt.format.colorspace = pix->colorspace;
    subdev_fmt.format.field = pix->field;

    switch (pix->pixelformat)
    {
        case V4L2_PIX_FMT_RGB565:
            subdev_fmt.format.code = MEDIA_BUS_FMT_RGB565_2X8_BE;
            break;
        case V4L2_PIX_FMT_YUYV:
            subdev_fmt.format.code = MEDIA_BUS_FMT_YUYV8_2X8;
            break;
        case V4L2_PIX_FMT_UYVY:
            subdev_fmt.format.code = MEDIA_BUS_FMT_UYVY8_2X8;
            break;
        case V4L2_PIX_FMT_YVYU:
            subdev_fmt.format.code = MEDIA_BUS_FMT_YVYU8_2X8;
            break;
        case V4L2_PIX_FMT_VYUY:
            subdev_fmt.format.code = MEDIA_BUS_FMT_VYUY8_2X8;
            break;
        default:
            subdev_fmt.format.code = MEDIA_BUS_FMT_UYVY8_2X8;
            break;
    }

    ret = v4l2_subdev_call(
      camdev->sensor_subdev, pad, set_fmt, &subdev_pad_config, &subdev_fmt);
    if (ret < 0)
    {
        dev_err(
          camdev->dev, "failed to call set format on subdevice: ret = %d\n",
          ret);
        goto set_fmt;
    }

    ret = __mzapo_cam_get_format(camdev, format);

set_fmt:
    return ret;
}

static int
mzapo_cam_set_format(struct file* file, void* fh, struct v4l2_format* format)
{
    struct mzapo_cam_device* camdev = video_drvdata(file);
    int ret;

    if (vb2_is_busy(&camdev->queue))
    {
        ret = __mzapo_cam_set_format(camdev, format, V4L2_SUBDEV_FORMAT_TRY);
        if (ret >= 0)
            ret = -EBUSY;
    }
    else
    {
        ret = __mzapo_cam_set_format(camdev, format, V4L2_SUBDEV_FORMAT_ACTIVE);
    }

    return ret;
}

static int
mzapo_cam_try_format(struct file* file, void* fh, struct v4l2_format* format)
{
    struct mzapo_cam_device* camdev = video_drvdata(file);
    int ret;

    ret = __mzapo_cam_set_format(camdev, format, V4L2_SUBDEV_FORMAT_TRY);

    return ret;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int mzapo_cam_get_register(
  struct file* file, void* fh, struct v4l2_dbg_register* reg)
{
    struct mzapo_cam_device* camdev = video_drvdata(file);
    int ret;

    ret = v4l2_subdev_call(camdev->sensor_subdev, core, g_register, reg);
    if (ret < 0)
    {
        dev_err(camdev->dev, "subdev g_register call failed: ret = %d\n", ret);
    }

    return ret;
}

static int mzapo_cam_set_register(
  struct file* file, void* fh, const struct v4l2_dbg_register* reg)
{
    struct mzapo_cam_device* camdev = video_drvdata(file);
    int ret;

    ret = v4l2_subdev_call(camdev->sensor_subdev, core, s_register, reg);
    if (ret < 0)
    {
        dev_err(camdev->dev, "subdev g_register call failed: ret = %d\n", ret);
    }

    return ret;
}
#endif

static const struct v4l2_ioctl_ops mzapo_cam_ioctl_ops = {
  // TODO
  .vidioc_querycap = mzapo_cam_querycap,
  //   .vidioc_enum_fmt_vid_cap = mzapo_cam_enum_format,
  .vidioc_g_fmt_vid_cap = mzapo_cam_get_format,
  .vidioc_s_fmt_vid_cap = mzapo_cam_set_format,
  .vidioc_try_fmt_vid_cap = mzapo_cam_try_format,
#ifdef CONFIG_VIDEO_ADV_DEBUG
  .vidioc_g_register = mzapo_cam_get_register,
  .vidioc_s_register = mzapo_cam_set_register,
#endif
  //   .vidioc_s_ctrl = mzapo_cam_s_ctrl,
  .vidioc_reqbufs = vb2_ioctl_reqbufs,
  .vidioc_querybuf = vb2_ioctl_querybuf,
  .vidioc_qbuf = vb2_ioctl_qbuf,
  .vidioc_dqbuf = vb2_ioctl_dqbuf,
  .vidioc_create_bufs = vb2_ioctl_create_bufs,
  .vidioc_expbuf = vb2_ioctl_expbuf,
  .vidioc_streamon = vb2_ioctl_streamon,
  .vidioc_streamoff = vb2_ioctl_streamoff,
};

static int mzapo_cam_v4l2_open(struct file* file)
{
    struct mzapo_cam_device* camdev = video_drvdata(file);
    int ret;

    mutex_lock(&camdev->lock);
    if (camdev->is_open)
    {
        ret = -EBUSY;
        goto unlock;
    }

    camdev->is_open = true;
    ret = v4l2_fh_open(file);
    if (ret < 0)
    {
        dev_err(camdev->dev, "failed to open file handle: ret = %d\n", ret);
        goto unlock;
    }

    // TODO power on
unlock:
    mutex_unlock(&camdev->lock);

    return ret;
}

static int mzapo_cam_v4l2_release(struct file* file)
{
    struct mzapo_cam_device* camdev = video_drvdata(file);
    int ret;

    mutex_lock(&camdev->lock);
    // TODO power off camera

    ret = v4l2_fh_release(file);
    camdev->is_open = false;
    mutex_unlock(&camdev->lock);

    return ret;
}

static const struct v4l2_file_operations mzapo_cam_fops = {
  .owner = THIS_MODULE,
  .open = mzapo_cam_v4l2_open,
  .release = mzapo_cam_v4l2_release,
  .poll = vb2_fop_poll,
  .mmap = vb2_fop_mmap,
  .read = vb2_fop_read,
  .unlocked_ioctl = video_ioctl2,
};

static int mzapo_cam_notify_bound(
  struct v4l2_async_notifier* notifier, struct v4l2_subdev* subdev,
  struct v4l2_async_subdev* asd)
{
    // HACK this function currently does not serve any purpose
    struct mzapo_cam_device* camdev =
      container_of(notifier, struct mzapo_cam_device, notifier);

    camdev->sensor_subdev = subdev;

    dev_info(camdev->dev, "sensor (%s) bound\n", subdev->name);

    return 0;
}

static int mzapo_cam_notify_complete(struct v4l2_async_notifier* notifier)
{
    int ret;
    struct mzapo_cam_device* camdev =
      container_of(notifier, struct mzapo_cam_device, notifier);
    struct v4l2_subdev* subdev;
    struct v4l2_subdev_format subdev_fmt = {0}; //HACK
    list_for_each_entry(subdev, &camdev->v4l2_dev.subdevs, list)
    {
        dev_info(camdev->dev, "list entry: %s\n", subdev->name);
    }

    ret = video_register_device(&camdev->video_dev, VFL_TYPE_GRABBER, -1);
    if (ret < 0)
    {
        dev_err(camdev->dev, "failed to register video\n");
        return ret;
    }

    // HACK
    ret = v4l2_subdev_call(camdev->sensor_subdev, core, s_power, 1);
    if (ret < 0)
    {
        dev_err(camdev->dev, "s_power failed: ret = %d\n", ret);
    }
    else
    {
        dev_info(camdev->dev, "s_power called\n");
    }

    ret =
      v4l2_subdev_call(camdev->sensor_subdev, pad, get_fmt, NULL, &subdev_fmt);
    if (ret < 0)
    {
        dev_err(camdev->dev, "get_fmt failed: ret = %d\n", ret);
    }
    else
    {
        dev_info(
          camdev->dev, "get_fmt: width = %d, height = %d, code = %d\n",
          subdev_fmt.format.width, subdev_fmt.format.height,
          subdev_fmt.format.code);
    }

    ret = v4l2_subdev_call(camdev->sensor_subdev, video, s_stream, 1);
    if (ret < 0)
    {
        dev_err(camdev->dev, "s_stream failed: ret = %d\n", ret);
    }
    else
    {
        dev_info(camdev->dev, "s_stream called\n");
    }

    ret = v4l2_device_register_subdev_nodes(&camdev->v4l2_dev);
    if (ret < 0)
    {
        dev_err(camdev->dev, "failed to register subdev nodes\n");
    }
    else
    {
        dev_info(camdev->dev, "subdev nodes registered\n");
    }
    // end HACK

    dev_info(camdev->dev, "notify complete call\n");
    return 0;
}

static const struct v4l2_async_notifier_operations mzapo_cam_notify_ops = {
  .bound = mzapo_cam_notify_bound,
  .complete = mzapo_cam_notify_complete,
};

static int mzapo_cam_probe(struct platform_device* pdev)
{
    struct mzapo_cam_device* camdev;
    struct v4l2_async_subdev** subdevs_arr;
    struct v4l2_async_subdev* subdev;
    struct resource* res;
    int ret;
    int irq;

    camdev = devm_kzalloc(&pdev->dev, sizeof(*camdev), GFP_KERNEL);
    if (!camdev)
        return -ENOMEM;

    camdev->dev = &pdev->dev;
    mutex_init(&camdev->lock);
    camdev->is_open = false;

    camdev->video_dev.fops = &mzapo_cam_fops;
    camdev->video_dev.v4l2_dev = &camdev->v4l2_dev;
    camdev->video_dev.queue = &camdev->queue;
    strlcpy(
      camdev->video_dev.name, "mzapo-camif", sizeof(camdev->video_dev.name));
    strlcpy(
      camdev->v4l2_dev.name, camdev->video_dev.name,
      sizeof(camdev->v4l2_dev.name));
    camdev->video_dev.vfl_type = VFL_TYPE_GRABBER;
    camdev->video_dev.vfl_dir = VFL_DIR_RX;
    camdev->video_dev.release = video_device_release_empty;
    camdev->video_dev.ioctl_ops = &mzapo_cam_ioctl_ops;
    camdev->video_dev.lock = &camdev->lock;
    camdev->video_dev.minor = -1;

    video_set_drvdata(&camdev->video_dev, camdev);

    camdev->queue.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    camdev->queue.io_modes =
      VB2_MMAP | VB2_READ; // TODO nema tady byt taky VB2_DMABUF?
    camdev->queue.dev = camdev->dev;
    camdev->queue.lock = &camdev->lock;
    camdev->queue.ops = &mzapo_cam_queue_ops;
    camdev->queue.mem_ops = &vb2_dma_contig_memops;
    camdev->queue.drv_priv = camdev;
    camdev->queue.timestamp_flags =
      V4L2_BUF_FLAG_TIMESTAMP_MONOTONIC | V4L2_BUF_FLAG_TSTAMP_SRC_EOF;
    // TODO buf_struct_size?

    ret = vb2_queue_init(&camdev->queue);
    if (ret < 0)
    {
        dev_err(camdev->dev, "failed to initialize VB2 queue\n");
        goto error_queue;
    }

    // TODO co je DMA channel a potrebuju to?

    // TODO pouzit reg-names misto poradi
    res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
    if (res == NULL)
    {
        dev_err(camdev->dev, "failed to read DMA regs\n");
        ret = -ENODEV;
        goto error_queue;
    }

    camdev->dma_reg = devm_ioremap_resource(camdev->dev, res);
    if (IS_ERR(camdev->dma_reg))
    {
        dev_err(camdev->dev, "failed to map DMA regs\n");
        ret = PTR_ERR(camdev->dma_reg);
        goto error_queue;
    }

    res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
    if (res == NULL)
    {
        dev_err(camdev->dev, "failed to read cam regs\n");
        ret = -ENODEV;
        goto error_queue;
    }

    camdev->cam_reg = devm_ioremap_resource(camdev->dev, res);
    if (IS_ERR(camdev->cam_reg))
    {
        dev_err(camdev->dev, "failed to map cam regs\n");
        ret = PTR_ERR(camdev->cam_reg);
        goto error_queue;
    }

    dev_info(camdev->dev, "cam regs\n");

    irq = platform_get_irq(pdev, 0);
    if (irq < 0)
    {
        dev_err(camdev->dev, "failed to read IRQ\n");
        goto error_queue;
    }

    ret = devm_request_irq(
      camdev->dev, irq, mzapo_cam_irq_handler, 0, "mzapocam",
      camdev); // TODO muze se sem dat IRQF_TRIGGER_RISING jako flag
    if (ret < 0)
    {
        dev_err(camdev->dev, "failed to request IRQ\n");
        goto error_queue;
    }

    dev_info(camdev->dev, "registered IRQ\n");

    ret = v4l2_device_register(camdev->dev, &camdev->v4l2_dev);
    if (ret < 0)
    {
        dev_err(camdev->dev, "failed to register V4L2 device (ret: %d)\n", ret);
        goto error_queue;
    }

    dev_info(camdev->dev, "registered device\n");

    camdev->sensor_node = of_parse_phandle(camdev->dev->of_node, "sensor", 0);
    if (camdev->sensor_node == NULL)
    {
        dev_err(camdev->dev, "failed to parse sensor\n");
        ret = -EINVAL;
        goto error_unreg;
    }

    dev_info(camdev->dev, "parsed phandle\n");

    subdevs_arr = devm_kzalloc(camdev->dev, sizeof(*subdevs_arr), GFP_KERNEL);
    if (subdevs_arr == NULL)
    {
        dev_err(camdev->dev, "subdevs array allocation failed\n");
        ret = -ENOMEM;
        goto error_node_put;
    }

    subdev =
      devm_kzalloc(camdev->dev, sizeof(struct v4l2_async_subdev), GFP_KERNEL);
    if (subdev == NULL)
    {
        dev_err(camdev->dev, "subdev allocation failed\n");
        ret = -ENOMEM;
        goto error_node_put;
    }
    dev_info(camdev->dev, "allocated subdevs\n");

    subdev->match_type = V4L2_ASYNC_MATCH_FWNODE;
    subdev->match.fwnode = of_fwnode_handle(camdev->sensor_node);
    subdevs_arr[0] = subdev;

    camdev->notifier.subdevs = subdevs_arr;
    camdev->notifier.num_subdevs = 1;
    camdev->notifier.ops = &mzapo_cam_notify_ops;

    dev_info(camdev->dev, "prepared subdevs\n");

    ret = v4l2_async_notifier_register(&camdev->v4l2_dev, &camdev->notifier);
    if (ret < 0)
    {
        dev_err(camdev->dev, "failed to register notifier\n");
        goto error_node_put;
    }

    platform_set_drvdata(pdev, camdev);

    dev_info(camdev->dev, "mzAPO-cam probed\n");

    return 0;

error_notifier:
    v4l2_async_notifier_unregister(&camdev->notifier);
error_node_put:
    of_node_put(camdev->sensor_node);
error_unreg:
    v4l2_device_unregister(&camdev->v4l2_dev);
error_queue:
    vb2_queue_release(&camdev->queue);
    mutex_destroy(&camdev->lock);
    return ret;
}

static int mzapo_cam_remove(struct platform_device* pdev)
{
    struct mzapo_cam_device* camdev = platform_get_drvdata(pdev);
    if (video_is_registered(&camdev->video_dev))
        video_unregister_device(&camdev->video_dev);
    v4l2_async_notifier_unregister(&camdev->notifier);
    of_node_put(camdev->sensor_node);
    v4l2_device_unregister(&camdev->v4l2_dev);
    vb2_queue_release(&camdev->queue);
    mutex_destroy(&camdev->lock);
    // TODO cleanup
    return 0;
}

static struct of_device_id mzapo_cam_of_id_table[] = {
  {.compatible = "ctu,mzapocam"},
  {},
};
MODULE_DEVICE_TABLE(of, mzapo_cam_of_id_table);

static struct platform_driver mzapo_cam_driver = {
  .driver =
    {
      .name = "mzapo-cam",
      .of_match_table = mzapo_cam_of_id_table,
    },
  .probe = mzapo_cam_probe,
  .remove = mzapo_cam_remove,
};

module_platform_driver(mzapo_cam_driver);

MODULE_AUTHOR("Asdf"); //TODO
MODULE_DESCRIPTION("mzAPO Camera Interface Driver for OV2640 Sensor");
MODULE_LICENSE("GPL"); //TODO
