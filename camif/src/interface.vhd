library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity interface is
  port( s_axi_lite_araddr : in std_logic_vector(9 downto 0);
        s_axi_lite_arready : out std_logic;
        s_axi_lite_arvalid : in std_logic;
        s_axi_lite_awaddr : in std_logic_vector(9 downto 0);
        s_axi_lite_awready : out std_logic;
        s_axi_lite_awvalid : in std_logic;
        s_axi_lite_bready : in std_logic;
        s_axi_lite_bresp : out std_logic_vector(1 downto 0);
        s_axi_lite_bvalid : out std_logic;
        s_axi_lite_rdata : out std_logic_vector(31 downto 0);
        s_axi_lite_rready : in std_logic;
        s_axi_lite_rresp : out std_logic_vector(1 downto 0);
        s_axi_lite_rvalid : out std_logic;
        s_axi_lite_wdata : in std_logic_vector(31 downto 0);
        s_axi_lite_wready : out std_logic;
        s_axi_lite_wvalid : in std_logic;
        axi_resetn : in std_logic;
        axi_aclk : in std_logic;
        
        m_axis_tdata : out std_logic_vector(7 downto 0);
        m_axis_tlast : out std_logic;
        m_axis_tready : in std_logic; -- HACK: ignoring tready
        m_axis_tvalid : out std_logic;
        
        cam_d : in std_logic_vector(7 downto 0);
        cam_hsync : in std_logic;
        cam_vsync : in std_logic;
        cam_valid : in std_logic;
        
        cam_reset : out std_logic;
        
        fifo_out_data : out std_logic_vector(9 downto 0);
        fifo_out_en : out std_logic;
        fifo_out_almost_full : in std_logic; 
        fifo_in_data : in std_logic_vector(9 downto 0);
        fifo_in_en : out std_logic;
        fifo_in_valid : in std_logic;
        fifo_reset : out std_logic );
end interface;

architecture behavioral of interface is
    type axi_state_t is (ADDRESS, DATA, OUTSIDE, RESPONSE);
    type cam_state_t is (CLEAR, STBY, SEND_FIRST, SEND);
    constant REG_ADDR : std_logic_vector(9 downto 0) := "0000000000";
    signal read_state : axi_state_t;
    signal write_state : axi_state_t;
    signal cam_state : cam_state_t;
    signal enable : std_logic;
    signal active : std_logic;
    signal reset : std_logic;
    signal cam_reset_sig : std_logic;
    signal m_axis_tdata_buf : std_logic_vector(m_axis_tdata'range);
    signal new_frame : std_logic;
begin
    cam_reset <= cam_reset_sig;

    process (axi_aclk, axi_resetn)
    begin
        if axi_resetn='0' then
            s_axi_lite_arready <= '0';
            s_axi_lite_rvalid <= '0';
            read_state <= ADDRESS;
        elsif rising_edge(axi_aclk) then
            case read_state is
            when ADDRESS =>
                if s_axi_lite_arvalid='1' then
                    s_axi_lite_arready <= '1';
                    if s_axi_lite_araddr=REG_ADDR then
                        read_state <= DATA;
                    else
                        read_state <= OUTSIDE;
                    end if;
                end if;
            when DATA =>
                s_axi_lite_arready <= '0';
                s_axi_lite_rdata <= (0=>enable, 1=>active, 2=>reset, 3=>cam_reset_sig, others=>'0');
                s_axi_lite_rdata <= (0=>enable, 1=>active, 2=>reset, others=>'0');
                s_axi_lite_rresp <= "00";
                s_axi_lite_rvalid <= '1';
                read_state <= RESPONSE;
            when OUTSIDE =>
                s_axi_lite_arready <= '0';
                s_axi_lite_rdata <= (others=>'0');
                s_axi_lite_rresp <= "00";
                s_axi_lite_rvalid <= '1';
                read_state <= RESPONSE;
            when RESPONSE =>
                if s_axi_lite_rready='1' then
                    s_axi_lite_rvalid <= '0';
                    read_state <= ADDRESS;
                end if;
            end case;
        end if; 
    end process;
    
    process (axi_aclk, axi_resetn)
    begin
        if axi_resetn='0' then
            s_axi_lite_awready <= '0';
            s_axi_lite_bvalid <= '0';
            s_axi_lite_wready <= '0';
            enable <= '0';
            reset <= '0';
            cam_reset_sig <= '1';
            write_state <= ADDRESS;
        elsif rising_edge(axi_aclk) then
            if active='1' then
                enable <= '0';
            else
                reset <= '0';
            end if;            
            case write_state is
            when ADDRESS =>
                if s_axi_lite_awvalid='1' then
                    s_axi_lite_awready <= '1';
                    if s_axi_lite_awaddr=REG_ADDR then
                        write_state <= DATA;
                    else
                        write_state <= OUTSIDE;
                    end if;
                end if;
            when DATA =>
                s_axi_lite_awready <= '0';
                if s_axi_lite_wvalid='1' then
                    s_axi_lite_wready <= '1';
                    enable <= s_axi_lite_wdata(0);
                    reset <= s_axi_lite_wdata(2);
                    cam_reset_sig <= s_axi_lite_wdata(3);
                    s_axi_lite_bresp <= "00";
                    s_axi_lite_bvalid <= '1';
                    write_state <= RESPONSE;
                end if;
            when OUTSIDE =>
                s_axi_lite_awready <= '0';
                if s_axi_lite_wvalid='1' then
                    s_axi_lite_wready <= '1';
                    s_axi_lite_bresp <= "00";
                    s_axi_lite_bvalid <= '1';
                    write_state <= RESPONSE;
                end if;
            when RESPONSE =>
                s_axi_lite_wready <= '0';
                if s_axi_lite_bready='1' then
                    s_axi_lite_bvalid <= '0';
                    write_state <= ADDRESS;
                end if;
            end case;
        end if;
    end process;
    
--    process (axi_aclk, axi_resetn)
--    begin
--        if axi_resetn='0' then
--            active <= '0';
--            m_axis_tlast <= '0';
--            m_axis_tvalid <= '0';
--            fifo_reset <= '1';
--            fifo_in_en <= '0';
--            cam_state <= CLEAR;
--        elsif rising_edge(axi_aclk) then
--            case cam_state is
--            when CLEAR =>
--                if cam_vsync='1' then
--                    fifo_reset <= '0';
--                    cam_state <= STBY;
--                end if;
--                fifo_out_en <= '0';
--            when STBY =>
--                m_axis_tvalid <= '0';
--                m_axis_tlast <= '0';
--                if enable='1' then
--                    fifo_in_en <= '1';
--                    active <= '1';
--                    cam_state <= SEND_FIRST;
--                elsif fifo_out_almost_full='1' then
--                    fifo_reset <= '1';
--                    cam_state <= CLEAR;
--                end if;
--                fifo_out_data <= cam_vsync & cam_hsync & cam_d;
--                fifo_out_en <= cam_valid;
--            when SEND_FIRST =>
--                if fifo_in_valid='1' and fifo_in_data(8)='1' then
--                    m_axis_tdata_buf <= fifo_in_data(7 downto 0);
--                    cam_state <= SEND; 
--                end if;
--                fifo_out_data <= cam_vsync & cam_hsync & cam_d;
--                fifo_out_en <= cam_valid;
--            when SEND =>
--                if fifo_in_valid='1' and fifo_in_data(9)='1' then -- VSYNC
--                    m_axis_tdata <= m_axis_tdata_buf;
--                    m_axis_tvalid <= '1';
--                    m_axis_tlast <= '1';
--                    active <= '0';
--                    fifo_in_en <= '0';
--                    cam_state <= STBY;
--                elsif fifo_in_valid='1' and fifo_in_data(8)='1' then -- HSYNC
--                    m_axis_tdata <= m_axis_tdata_buf;
--                    m_axis_tdata_buf <= fifo_in_data(7 downto 0); 
--                    m_axis_tvalid <= '1';
--                else
--                    m_axis_tvalid <= '0';
--                end if;
--                fifo_out_data <= cam_vsync & cam_hsync & cam_d;
--                fifo_out_en <= cam_valid;
--            end case;
--        end if;
--    end process;

    process (axi_aclk, axi_resetn) -- without FIFO buffer
    begin
        if axi_resetn='0' then
            active <= '0';
            m_axis_tlast <= '0';
            m_axis_tvalid <= '0';
            cam_state <= CLEAR;
        elsif rising_edge(axi_aclk) then
            case cam_state is
            when CLEAR =>
                if cam_valid='1' and cam_vsync='1' then
                    cam_state <= STBY;
                end if;
            when STBY =>
                m_axis_tvalid <= '0';
                m_axis_tlast <= '0';
                if cam_valid='1' and cam_vsync='0' then
                    cam_state <= CLEAR;
                elsif enable='1' then
                    active <= '1';
                    cam_state <= SEND_FIRST;
                end if;
            when SEND_FIRST =>
                if reset='1' then
                    active <= '0';
                    cam_state <= CLEAR;
                elsif cam_valid='1' and cam_hsync='1' then
                    m_axis_tdata_buf <= cam_d;
                    cam_state <= SEND;
                end if;
            when SEND =>
                if reset='1' then
                    m_axis_tvalid <= '0';
                    active <= '0';
                    cam_state <= CLEAR;
                elsif cam_valid='1' and cam_vsync='1' then
                    m_axis_tdata <= m_axis_tdata_buf;
                    m_axis_tvalid <= '1';
                    m_axis_tlast <= '1';
                    active <= '0';
                    cam_state <= STBY;
                elsif cam_valid='1' and cam_hsync='1' then -- HSYNC
                    m_axis_tdata <= m_axis_tdata_buf;
                    m_axis_tdata_buf <= cam_d; 
                    m_axis_tvalid <= '1';
                else
                    m_axis_tvalid <= '0';
                end if;
            end case;
        end if;
    end process;

--    process (axi_aclk, axi_resetn) -- test cam
--    begin
--        if axi_resetn='0' then
--            active <= '0';
--            m_axis_tlast <= '0';
--            m_axis_tvalid <= '0';
--            cam_state <= CLEAR;
--        elsif rising_edge(axi_aclk) then
--            if enable='1' then
--                active <= '0';
--            elsif cam_hsync='1' then
--                active <= '1';
--            end if;
--        end if;
--    end process;
    
--    process (axi_aclk, axi_resetn) -- test write
--        variable delay : integer range 0 to 15;
--    begin
--        if axi_resetn='0' then
--            active <= '0';
--            m_axis_tlast <= '0';
--            m_axis_tvalid <= '0';
--            cam_state <= CLEAR;
--            delay := 0;
--        elsif rising_edge(axi_aclk) then
--            case cam_state is
--            when CLEAR =>
--                if delay=0 then
--                    cam_state <= STBY;
--                end if;
--            when STBY =>
--                m_axis_tvalid <= '0';
--                m_axis_tlast <= '0';
--                if delay=4 then
--                    cam_state <= CLEAR;
--                elsif enable='1' then
--                    active <= '1';
--                    cam_state <= SEND_FIRST;
--                end if;
--            when SEND_FIRST =>
--                if delay=4 then
--                    m_axis_tdata_buf <= x"AE";
--                    cam_state <= SEND; 
--                end if;
--            when SEND =>
--                if delay=0 then
--                    m_axis_tdata <= m_axis_tdata_buf;
--                    m_axis_tvalid <= '1';
--                    m_axis_tlast <= '1';
--                    active <= '0';
--                    cam_state <= STBY;
--                elsif delay=6 or delay=11 or delay=12 then -- HSYNC
--                    m_axis_tdata <= m_axis_tdata_buf;
--                    m_axis_tdata_buf <= std_logic_vector(to_unsigned(delay, 8)); 
--                    m_axis_tvalid <= '1';
--                else
--                    m_axis_tvalid <= '0';
--                end if;
--            end case;
--            delay := (delay + 1) mod 16; 
--        end if;
--    end process;
end behavioral;
